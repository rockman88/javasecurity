package com.hd;

import static org.apache.commons.codec.binary.Hex.encodeHex;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;


public class Main {
	// TODO:
	// https://stackoverflow.com/questions/37388680/in-java-how-to-convert-correctly-byte-to-string-to-byte-again
	public static void main(String[] args) throws Exception {
		javaCrypto("Hello World!");
	}

	// TODO: deal with exception properly
	private static void javaCrypto(String plainTxt) throws Exception {
		System.out.println("********** JAVA CRYPTO ************");
		
		Base64 b64 = new Base64();

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		
		SecretKey key = genSymetricSecretKey();
		
		// encode key if we want to store it...
		System.out.println("Key encode Hex: " + String.valueOf(encodeHex(key.getEncoded())));
		
		// https://docs.oracle.com/javase/9/docs/specs/security/standard-names.html#securerandom-number-generation-algorithms
		SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
		byte[] iv = new byte[cipher.getBlockSize()];
		randomSecureRandom.nextBytes(iv);

		IvParameterSpec ivParams = new IvParameterSpec(iv);

		cipher.init(Cipher.ENCRYPT_MODE, key, ivParams);

		byte[] plainText = plainTxt.getBytes(StandardCharsets.UTF_8);
		byte[] cipherText = cipher.doFinal(plainText);
		
		System.out.println("Cipher Text Size: " + cipherText.length);

		// Base64 encode the encrypted data so we can send it to people
		String cipherTextBase64 = new String(b64.encode(cipherText));
		
		System.out.println("Ecrypted Value (Base64): " + cipherTextBase64);
		System.out.println("Ecrypted Value (Base64) size: " + cipherTextBase64.length());
		
		// Base64 Decode the encrypted data
		byte[] cipherText2 = b64.decode(cipherTextBase64);
		
		cipher.init(Cipher.DECRYPT_MODE, key, ivParams);
		
		byte[] decryptedText = cipher.doFinal(cipherText2);
		
		System.out.println("Decrypted Value: " + new String(decryptedText));

	}

	private static SecretKey genSymetricSecretKey() throws NoSuchAlgorithmException {
		Base64 b64 = new Base64();
		
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");

		SecureRandom secureRandom = new SecureRandom();
		int keyBitSize = 256;
		keyGenerator.init(keyBitSize, secureRandom);

		SecretKey secretKey = keyGenerator.generateKey();

		System.out.println("genSymetricSecretKey: " + new String(b64.encode(secretKey.getEncoded())));

		return secretKey;
	}
}
